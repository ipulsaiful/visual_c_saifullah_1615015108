/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package package1;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
/**
 *
 * @author USER
 */
public class Tampilrestoran extends javax.swing.JFrame {
    
    
    private DefaultTableModel model;
    private Connection con = koneksi.getConnection();
    private Statement stt; //untuk eksekusi query database
    private ResultSet rss; //untuk penampung data dari database
    /**
     * Creates new form Tampilrestoran
     */
    public Tampilrestoran() {
        initComponents();
    }
    
    private void InitTableMakanan(){
    model = new DefaultTableModel();
    model.addColumn("ID MAKANAN");
    model.addColumn("NAMA MAKANAN");
    model.addColumn("JENIS HIDANGAN");
    model.addColumn("HARGA");
    
    tablemakanan.setModel(model);
    }
    private void InitTableMinuman(){
    model = new DefaultTableModel();
    model.addColumn("ID MINUMAN");
    model.addColumn("NAMA MINUMAN");
    model.addColumn("HARGA");
    
    tableminuman.setModel(model);
    }
    
    private void DataTableMakanan(){
        InitTableMakanan();
        TampilDataMakanan();
    }
    
    private void DataTableMinuman(){
        InitTableMinuman();
        TampilDataMinuman();
        
    }
    
    private void TampilDataMakanan(){
       try{
           String sql = "SELECT * FROM makanan";
           stt = con.createStatement();
           rss = stt.executeQuery(sql);
           while(rss.next()){
               Object[] o = new Object[4];
               o[0] = rss.getString("id");
               o[1] = rss.getString("nama");
               o[2] = rss.getString("jenis_hidangan");
               o[3] = rss.getInt("harga");
               model.addRow(o);
           }
       }catch(SQLException e){
           System.out.println(e.getMessage());
       }
    }
    
    private void TampilDataMinuman(){
       try{
           String sql = "SELECT * FROM minuman";
           stt = con.createStatement();
           rss = stt.executeQuery(sql);
           while(rss.next()){
               Object[] o = new Object[3];
               o[0] = rss.getString("id");
               o[1] = rss.getString("nama");
               o[2] = rss.getInt("harga");
               model.addRow(o);
           }
       }catch(SQLException e){
           System.out.println(e.getMessage());
       }
    }
    
   
    private void PencarianDataMakanan(String by,String cari){
       by = bymakanan.getSelectedItem().toString();
       try{
            String sql = "SELECT * FROM makanan WHERE "+by+" LIKE '%"+cari+"%';";
            stt = con.createStatement();
            rss = stt.executeQuery(sql);
            while(rss.next()){
                Object[] data = new Object[4];
                data[0] = rss.getString("id");
                data[1] = rss.getString("nama");
                data[2] = rss.getString("jenis_hidangan");
                data[3] = rss.getInt("harga");
                model.addRow(data);
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    private void PencarianDataMinuman(String by,String cari){
       by = Byminuman.getSelectedItem().toString();
       try{
            String sql = "SELECT * FROM minuman WHERE "+by+" LIKE '%"+cari+"%';";
            stt = con.createStatement();
            rss = stt.executeQuery(sql);
            while(rss.next()){
                Object[] data = new Object[3];
                data[0] = rss.getString("id");
                data[1] = rss.getString("nama");
                data[2] = rss.getInt("harga");
                model.addRow(data);
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    private void UrutDataMakanan(String by,String metode){
       by = Sortby.getSelectedItem().toString();
       try{
            String sql = "SELECT * FROM makanan ORDER BY "+by+" "+metode+";";
            stt = con.createStatement();
            rss = stt.executeQuery(sql);
            while(rss.next()){
                Object[] data = new Object[4];
                data[0] = rss.getString("id");
                data[1] = rss.getString("nama");
                data[2] = rss.getString("jenis_hidangan");
                data[3] = rss.getInt("harga");
                model.addRow(data);
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    private void UrutDataMinuman(String by,String metode){
       by = Sortbyminuman.getSelectedItem().toString();
       try{
            String sql = "SELECT * FROM minuman ORDER BY "+by+" "+metode+";";
            stt = con.createStatement();
            rss = stt.executeQuery(sql);
            while(rss.next()){
                Object[] data = new Object[3];
                data[0] = rss.getString("id");
                data[1] = rss.getString("nama");
                data[2] = rss.getInt("harga");
                model.addRow(data);
            }
        }catch(Exception e){
            System.out.println(e.getMessage());
        }
    }
    
    
    private boolean UbahDataMakanan(String id,String nama, String jenis, String harga){
       try{
           String sql = "UPDATE makanan set nama='"+nama+"', jenis_hidangan='"+jenis+"', harga="+harga+" WHERE id="+id+";";
           stt = con.createStatement();
           stt.executeUpdate(sql);
           return true;
       }catch(SQLException e){
           System.out.println(e.getMessage());
           return false;
       }
   }
    private boolean UbahDataMinuman(String id,String nama, String harga){
       try{
           String sql = "UPDATE minuman set nama='"+nama+"', harga="+harga+" WHERE id="+id+";";
           stt = con.createStatement();
           stt.executeUpdate(sql);
           return true;
       }catch(SQLException e){
           System.out.println(e.getMessage());
           return false;
       }
   }
    private boolean HapusDataMakanan(String id){
       try{
        String sql = "DELETE FROM makanan WHERE id="+id+";";
        stt = con.createStatement();
        stt.executeUpdate(sql);
        return true;
       }catch(SQLException e){
           System.out.println(e.getMessage());   
           return false;
       }
   }
    private boolean HapusDataMinuman(String id){
       try{
        String sql = "DELETE FROM minuman WHERE id="+id+";";
        stt = con.createStatement();
        stt.executeUpdate(sql);
        return true;
       }catch(SQLException e){
           System.out.println(e.getMessage());   
           return false;
       }
   }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jPanel3 = new javax.swing.JPanel();
        jPanel2 = new javax.swing.JPanel();
        panel_minuman = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        tableminuman = new javax.swing.JTable();
        editminuman = new javax.swing.JButton();
        deleteminuman = new javax.swing.JButton();
        cariminuman = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        Byminuman = new javax.swing.JComboBox<>();
        jLabel7 = new javax.swing.JLabel();
        Sortbyminuman = new javax.swing.JComboBox<>();
        asc1 = new javax.swing.JRadioButton();
        desc1 = new javax.swing.JRadioButton();
        submitminuman = new javax.swing.JButton();
        nama_minuman = new javax.swing.JTextField();
        harga_minuman = new javax.swing.JTextField();
        jLabel11 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        panel_makanan = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablemakanan = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        deletemakanan = new javax.swing.JButton();
        carimakanani = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        bymakanan = new javax.swing.JComboBox<>();
        jLabel4 = new javax.swing.JLabel();
        Sortby = new javax.swing.JComboBox<>();
        asc = new javax.swing.JRadioButton();
        desc = new javax.swing.JRadioButton();
        submit = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        nama_makanan = new javax.swing.JTextField();
        Jenis_Hidangan = new javax.swing.JComboBox<>();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        Harga_Makanan = new javax.swing.JTextField();
        jLabel14 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jPanel4 = new javax.swing.JPanel();
        TampilpanelF = new javax.swing.JButton();
        TampilpanelD = new javax.swing.JButton();
        TambahData = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        addComponentListener(new java.awt.event.ComponentAdapter() {
            public void componentShown(java.awt.event.ComponentEvent evt) {
                formComponentShown(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(0, 153, 204));

        jPanel2.setLayout(new java.awt.CardLayout());

        panel_minuman.setBackground(new java.awt.Color(153, 0, 255));

        tableminuman.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tableminuman.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tableminumanMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tableminuman);

        editminuman.setText("edit");
        editminuman.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editminumanActionPerformed(evt);
            }
        });

        deleteminuman.setText("delete");
        deleteminuman.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deleteminumanActionPerformed(evt);
            }
        });

        cariminuman.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                cariminumanCaretUpdate(evt);
            }
        });
        cariminuman.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cariminumanActionPerformed(evt);
            }
        });

        jLabel5.setText("Search :");

        jLabel6.setText("By :");

        Byminuman.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nama", "jenis_hidangan", "harga" }));

        jLabel7.setText("Sort by :");

        Sortbyminuman.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "id", "nama", "harga" }));

        buttonGroup1.add(asc1);
        asc1.setText("ASC");

        buttonGroup1.add(desc1);
        desc1.setText("DESC");

        submitminuman.setText("submit");
        submitminuman.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitminumanActionPerformed(evt);
            }
        });

        jLabel11.setText("Nama :");

        jLabel12.setText("Harga :");

        jLabel13.setText("Minuman");

        javax.swing.GroupLayout panel_minumanLayout = new javax.swing.GroupLayout(panel_minuman);
        panel_minuman.setLayout(panel_minumanLayout);
        panel_minumanLayout.setHorizontalGroup(
            panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_minumanLayout.createSequentialGroup()
                .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_minumanLayout.createSequentialGroup()
                        .addGap(37, 37, 37)
                        .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_minumanLayout.createSequentialGroup()
                        .addGap(103, 103, 103)
                        .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel5)
                            .addComponent(jLabel7)
                            .addComponent(jLabel11)
                            .addComponent(jLabel12))
                        .addGap(18, 18, 18)
                        .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(harga_minuman, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panel_minumanLayout.createSequentialGroup()
                                .addGap(73, 73, 73)
                                .addComponent(jLabel13))
                            .addComponent(nama_minuman, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panel_minumanLayout.createSequentialGroup()
                                .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panel_minumanLayout.createSequentialGroup()
                                        .addComponent(cariminuman, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jLabel6))
                                    .addComponent(Sortbyminuman, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(18, 18, 18)
                                .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(panel_minumanLayout.createSequentialGroup()
                                        .addComponent(asc1)
                                        .addGap(18, 18, 18)
                                        .addComponent(desc1))
                                    .addComponent(Byminuman, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE))))))
                .addGap(18, 18, 18)
                .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(submitminuman)
                    .addComponent(deleteminuman)
                    .addComponent(editminuman, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(55, Short.MAX_VALUE))
        );
        panel_minumanLayout.setVerticalGroup(
            panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_minumanLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel5)
                    .addComponent(cariminuman, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel6)
                    .addComponent(Byminuman, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(Sortbyminuman, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(desc1)
                        .addComponent(submitminuman)
                        .addComponent(asc1))
                    .addComponent(jLabel7))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 83, Short.MAX_VALUE)
                .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nama_minuman, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel11))
                .addGap(18, 18, 18)
                .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(harga_minuman, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel12))
                .addGap(18, 18, 18)
                .addComponent(jLabel13)
                .addGap(15, 15, 15)
                .addGroup(panel_minumanLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(panel_minumanLayout.createSequentialGroup()
                        .addComponent(editminuman)
                        .addGap(18, 18, 18)
                        .addComponent(deleteminuman)))
                .addContainerGap())
        );

        jPanel2.add(panel_minuman, "card4");

        panel_makanan.setBackground(new java.awt.Color(153, 0, 255));

        tablemakanan.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablemakanan.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablemakananMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablemakanan);

        jButton1.setText("edit");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        deletemakanan.setText("delete");
        deletemakanan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                deletemakananActionPerformed(evt);
            }
        });

        carimakanani.addCaretListener(new javax.swing.event.CaretListener() {
            public void caretUpdate(javax.swing.event.CaretEvent evt) {
                carimakananiCaretUpdate(evt);
            }
        });
        carimakanani.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                carimakananiActionPerformed(evt);
            }
        });

        jLabel2.setText("Search :");

        jLabel3.setText("By :");

        bymakanan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "nama", "jenis_hidangan", "harga" }));

        jLabel4.setText("Sort by :");

        Sortby.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "id", "nama", "jenis_hidangan", "harga" }));

        buttonGroup1.add(asc);
        asc.setText("ASC");

        buttonGroup1.add(desc);
        desc.setText("DESC");

        submit.setText("submit");
        submit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitActionPerformed(evt);
            }
        });

        jLabel8.setText("Nama :");

        nama_makanan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                nama_makananActionPerformed(evt);
            }
        });

        Jenis_Hidangan.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Hidangan Pembuka", "Hidangan Utama", "Hidangan Penutup" }));

        jLabel9.setText("Jenis Hidangan :");

        jLabel10.setText("Harga :");

        Harga_Makanan.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Harga_MakananActionPerformed(evt);
            }
        });

        jLabel14.setText("Makanan");

        javax.swing.GroupLayout panel_makananLayout = new javax.swing.GroupLayout(panel_makanan);
        panel_makanan.setLayout(panel_makananLayout);
        panel_makananLayout.setHorizontalGroup(
            panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_makananLayout.createSequentialGroup()
                .addContainerGap(90, Short.MAX_VALUE)
                .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(panel_makananLayout.createSequentialGroup()
                        .addComponent(jLabel2)
                        .addGap(18, 18, 18)
                        .addComponent(carimakanani, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(27, 27, 27)
                        .addComponent(jLabel3)
                        .addGap(18, 18, 18)
                        .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(bymakanan, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(panel_makananLayout.createSequentialGroup()
                                .addComponent(asc)
                                .addGap(18, 18, 18)
                                .addComponent(desc)
                                .addGap(18, 18, 18)
                                .addComponent(submit)))
                        .addGap(47, 47, 47))
                    .addGroup(panel_makananLayout.createSequentialGroup()
                        .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel4)
                            .addComponent(jLabel8)
                            .addComponent(jLabel9)
                            .addComponent(jLabel10))
                        .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(panel_makananLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(Jenis_Hidangan, 0, 132, Short.MAX_VALUE)
                                    .addComponent(nama_makanan)
                                    .addComponent(Sortby, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_makananLayout.createSequentialGroup()
                                .addGap(18, 18, 18)
                                .addComponent(Harga_Makanan, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(306, 306, 306))))
            .addGroup(panel_makananLayout.createSequentialGroup()
                .addGap(247, 247, 247)
                .addComponent(jLabel14)
                .addGap(112, 112, 112)
                .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jButton1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(deletemakanan, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panel_makananLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1)
                .addContainerGap())
        );
        panel_makananLayout.setVerticalGroup(
            panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_makananLayout.createSequentialGroup()
                .addGap(21, 21, 21)
                .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(carimakanani, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel3)
                    .addComponent(bymakanan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(Sortby, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(asc)
                    .addComponent(desc)
                    .addComponent(submit))
                .addGap(40, 40, 40)
                .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(nama_makanan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel8))
                .addGap(18, 18, 18)
                .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(Jenis_Hidangan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel9))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 15, Short.MAX_VALUE)
                .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jButton1)
                    .addComponent(Harga_Makanan, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel10))
                .addGap(18, 18, 18)
                .addGroup(panel_makananLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(deletemakanan)
                    .addComponent(jLabel14))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 91, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );

        jPanel2.add(panel_makanan, "card3");

        jPanel1.setBackground(new java.awt.Color(0, 102, 102));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/package1/resto.png"))); // NOI18N

        jPanel4.setLayout(new java.awt.GridLayout());

        TampilpanelF.setText("makanan");
        TampilpanelF.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TampilpanelFActionPerformed(evt);
            }
        });
        jPanel4.add(TampilpanelF);

        TampilpanelD.setText("minuman");
        TampilpanelD.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TampilpanelDActionPerformed(evt);
            }
        });
        jPanel4.add(TampilpanelD);

        TambahData.setText("tambahdata");
        TambahData.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                TambahDataActionPerformed(evt);
            }
        });
        jPanel4.add(TambahData);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel1)
                .addGap(27, 27, 27))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(36, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void carimakananiActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_carimakananiActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_carimakananiActionPerformed

    private void formComponentShown(java.awt.event.ComponentEvent evt) {//GEN-FIRST:event_formComponentShown
        // TODO add your handling code here:
        DataTableMakanan();
        DataTableMinuman();
    }//GEN-LAST:event_formComponentShown

    private void carimakananiCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_carimakananiCaretUpdate
        // TODO add your handling code here:
        InitTableMakanan();
        if(carimakanani.getText().length()==0){
            TampilDataMakanan();
        }else{
            PencarianDataMakanan(bymakanan.getSelectedItem().toString(),carimakanani.getText());
        }
    
    }//GEN-LAST:event_carimakananiCaretUpdate

    private void submitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitActionPerformed
        // TODO add your handling code here:
        InitTableMakanan();
        String BY;
        String metode;
        BY = Sortby.getSelectedItem().toString();
        if(asc.isSelected()){
            metode = "ASC";
        }else if(desc.isSelected()){
            metode = "DESC";
        }else{
            metode = "ASC";
        }
        
        UrutDataMakanan(BY, metode);
        
    }//GEN-LAST:event_submitActionPerformed

    private void cariminumanCaretUpdate(javax.swing.event.CaretEvent evt) {//GEN-FIRST:event_cariminumanCaretUpdate
        // TODO add your handling code here:
        InitTableMinuman();
        if(cariminuman.getText().length()==0){
            TampilDataMinuman();
        }else{
            PencarianDataMinuman(Byminuman.getSelectedItem().toString(),cariminuman.getText());
        }
    }//GEN-LAST:event_cariminumanCaretUpdate

    private void cariminumanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cariminumanActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cariminumanActionPerformed

    private void submitminumanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitminumanActionPerformed
        // TODO add your handling code here:
        InitTableMinuman();
        String BY;
        String metode;
        BY = Sortbyminuman.getSelectedItem().toString();
        if(asc1.isSelected()){
            metode = "ASC";
        }else if(desc1.isSelected()){
            metode = "DESC";
        }else{
            metode = "ASC";
        }
        
        UrutDataMinuman(BY, metode);
        
    }//GEN-LAST:event_submitminumanActionPerformed

    private void tableminumanMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tableminumanMouseClicked
        // TODO add your handling code here:
        int baris = tableminuman.getSelectedRow();
        nama_minuman.setText(tableminuman.getValueAt(baris, 1).toString());
        harga_minuman.setText(tableminuman.getValueAt(baris,2).toString());
        
    }//GEN-LAST:event_tableminumanMouseClicked

    private void tablemakananMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablemakananMouseClicked
        // TODO add your handling code here:
        int baris = tablemakanan.getSelectedRow();
        nama_makanan.setText(tablemakanan.getValueAt(baris, 1).toString());
        Jenis_Hidangan.setSelectedItem(tablemakanan.getValueAt(baris, 2).toString());
        Harga_Makanan.setText(tablemakanan.getValueAt(baris,3).toString());
    }//GEN-LAST:event_tablemakananMouseClicked

    private void nama_makananActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_nama_makananActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_nama_makananActionPerformed

    private void Harga_MakananActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Harga_MakananActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_Harga_MakananActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        // TODO add your handling code here:
        int baris = tablemakanan.getSelectedRow();
        String id = tablemakanan.getValueAt(baris, 0).toString();
        String namamakanan = nama_makanan.getText();
        String jenis = Jenis_Hidangan.getSelectedItem().toString();
        String hargamakanan = Harga_Makanan.getText();
        if(UbahDataMakanan(id, namamakanan, jenis, hargamakanan))
            JOptionPane.showMessageDialog(null, "berhasil ubah data");
        else
            JOptionPane.showConfirmDialog(null, "gagal ubah data");
        DataTableMakanan();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void deletemakananActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deletemakananActionPerformed
        // TODO add your handling code here:
        int baris = tablemakanan.getSelectedRow();
        String id = tablemakanan.getValueAt(baris, 0).toString();
        if(HapusDataMakanan(id))
            JOptionPane.showMessageDialog(null, "berhasil hapus data");
        else
            JOptionPane.showConfirmDialog(null, "gagal hapus data");
        DataTableMakanan();
    }//GEN-LAST:event_deletemakananActionPerformed

    private void editminumanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editminumanActionPerformed
        // TODO add your handling code here:
        int baris = tableminuman.getSelectedRow();
        String id = tableminuman.getValueAt(baris, 0).toString();
        String namaminuman = nama_minuman.getText();
        String hargaminuman = harga_minuman.getText();
        if(UbahDataMinuman(id, namaminuman, hargaminuman))
            JOptionPane.showMessageDialog(null, "berhasil ubah data");
        else
            JOptionPane.showConfirmDialog(null, "gagal ubah data");
        DataTableMinuman();
    }//GEN-LAST:event_editminumanActionPerformed

    private void deleteminumanActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_deleteminumanActionPerformed
        // TODO add your handling code here:
        int baris = tableminuman.getSelectedRow();
        String id = tableminuman.getValueAt(baris, 0).toString();
        if(HapusDataMinuman(id))
            JOptionPane.showMessageDialog(null, "berhasil hapus data");
        else
            JOptionPane.showConfirmDialog(null, "gagal hapus data");
        DataTableMinuman();
    }//GEN-LAST:event_deleteminumanActionPerformed

    private void TampilpanelFActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TampilpanelFActionPerformed
        // TODO add your handling code here:
        panel_makanan.setVisible(true);
        panel_minuman.setVisible(false);
    }//GEN-LAST:event_TampilpanelFActionPerformed

    private void TampilpanelDActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TampilpanelDActionPerformed
        // TODO add your handling code here:
        panel_makanan.setVisible(false);
        panel_minuman.setVisible(true);
    }//GEN-LAST:event_TampilpanelDActionPerformed

    private void TambahDataActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_TambahDataActionPerformed
        // TODO add your handling code here:
        new FormRestorant().setVisible(true);
        this.dispose();
    }//GEN-LAST:event_TambahDataActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Tampilrestoran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Tampilrestoran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Tampilrestoran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Tampilrestoran.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Tampilrestoran().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JComboBox<String> Byminuman;
    private javax.swing.JTextField Harga_Makanan;
    private javax.swing.JComboBox<String> Jenis_Hidangan;
    private javax.swing.JComboBox<String> Sortby;
    private javax.swing.JComboBox<String> Sortbyminuman;
    private javax.swing.JButton TambahData;
    private javax.swing.JButton TampilpanelD;
    private javax.swing.JButton TampilpanelF;
    private javax.swing.JRadioButton asc;
    private javax.swing.JRadioButton asc1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JComboBox<String> bymakanan;
    private javax.swing.JTextField carimakanani;
    private javax.swing.JTextField cariminuman;
    private javax.swing.JButton deletemakanan;
    private javax.swing.JButton deleteminuman;
    private javax.swing.JRadioButton desc;
    private javax.swing.JRadioButton desc1;
    private javax.swing.JButton editminuman;
    private javax.swing.JTextField harga_minuman;
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField nama_makanan;
    private javax.swing.JTextField nama_minuman;
    private javax.swing.JPanel panel_makanan;
    private javax.swing.JPanel panel_minuman;
    private javax.swing.JButton submit;
    private javax.swing.JButton submitminuman;
    private javax.swing.JTable tablemakanan;
    private javax.swing.JTable tableminuman;
    // End of variables declaration//GEN-END:variables
}
